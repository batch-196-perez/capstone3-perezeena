import {useState,useEffect, useContext} from 'react';
import {Table, Container, Card, Button, Col, Modal, Form} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import {Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function ProductCard({productProp}){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const {name, description, price, _id, isActive} = productProp;
	//console.log(productProp._id);

	// for Edit 
	const [showEdit, setShowEdit] = useState(false);
	const handleCloseEdit = () => setShowEdit(false);
	const handleShowEdit = () => setShowEdit(true);

	// for Activate
	const [activeProduct, setActiveProduct] = useState(true)

	// for delete
	const [showDelete, setShowDelete] = useState()
	const handleCloseDel = () => setShowDelete(false);
	const handleShowDel = () => setShowDelete(true);

	// const id = productProp.id;

	const [productName, setProductName] = useState(productProp.name);
	const [productDescription, setProductDescription] = useState(productProp.description);
	const [productPrice, setProductPrice] = useState(productProp.price);
	const [productIsActive, setProductisActive] = useState(isActive);



	const updateProduct = (_id) => {	
		fetch(`http://localhost:4000/products/updateProduct/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: productName,
				description: productDescription,
				price: productPrice
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
		
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
			setProductName(name);
			setProductDescription(description);
			setProductPrice(price);
		})

	}

	//Activating

	function activateProduct(_id){

		fetch(`http://localhost:4000/products/activateProduct/${_id}`,{
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
		})
		setProductisActive(true);
	}

	//Deactivate
	function deactivateProduct(_id){

		fetch(`http://localhost:4000/products/archiveProduct/${_id}`,{
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
		 			title: 'Product Info Updated',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});
		})
		setProductisActive(false);
	}

	const del = (_id) => {
			fetch(`http://localhost:4000/products/deleteProduct/${_id}`,{
			method: 'DELETE',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			Swal.fire({
		 			title: 'Deleted Product',
		 			icon: 'success',
		 			text: 'The product is already updated'
		 		});

		})
		}

	useEffect(() => {
				
		

	},[])


	return (
		<>
		<tr variant="primary">
          <td>{name}</td>
          <td>{description}</td>
          <td>{price}</td>
          <td>{isActive.toString()}</td>
          <td><Button variant="danger" onClick={handleShowDel}>Delete</Button></td>
          <td><Button variant="success" onClick={handleShowEdit}>Update</Button></td>
          <td>
          { isActive ? 
             <Button variant="outline-warning" onClick={e => deactivateProduct(_id)}>Deactive</Button>
            :
             <Button variant="warning" onClick={e => activateProduct(_id)}>Activate</Button>
          }
          </td>
        </tr>

		<Modal show={showEdit} onHide={handleCloseEdit} >
			        <Modal.Header closeButton>
			        <Modal.Title>Update Product's Info</Modal.Title>
					</Modal.Header>
					<Modal.Body>
						<Form >
							<Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Name</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productName}
						        onChange = {e => setProductName(e.target.value)}
						        autoFocus
						    />
							</Form.Group>

						    <Form.Group className="mb-3" controlId="productName">
						    <Form.Label>Description</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productDescription}
						        onChange = {e => setProductDescription(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="productPrice">
						    <Form.Label>Price</Form.Label>
						    <Form.Control
						        type="text"
						        placeholder=" "
						        value = {productPrice}
						        onChange = {e => setProductPrice(e.target.value)}
						        autoFocus
						    />
						    </Form.Group>

						</Form>
						</Modal.Body>
					<Modal.Footer>
					    <Button variant="secondary" onClick={handleCloseEdit}>
					    Close
					    </Button>
					    <Button variant="primary" onClick={() => updateProduct(_id)}>
					    Save Changes
					    </Button>
					</Modal.Footer>
			</Modal> 


			<Modal show={showDelete} onHide={handleCloseDel}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you will delete this product?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDel}>
            Close
          </Button>
          <Button variant="primary" onClick={() => del(_id)}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
			</>
	)
}