


import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [name, setName]= useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [totalAmount, setTotalAmount] = useState("");
	const [quantity, setQuantity] = useState(1);

	const {productId} = useParams();

	const buy = (productId) => {

		fetch('http://localhost:4000/users/addOrder',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				totalAmount : price * quantity,
				products: [
					{
						productId: productId,
						quantity: quantity,
						productName: name
					}
				]
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
					title: 'Successfully ordered!',
					icon: 'success',
					text: 'Thank you for ordering'
				});

				history("/products");

			} else {
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				});
			};

		});
	};

	useEffect(() => {
		console.log(productId)
		fetch(`http://localhost:4000/products/singleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			
		});

	}, [productId]);

	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{span:6, offset:3}}>
					<Card>
						<Card.Body>
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>Php {price}</Card.Text>
							{ user.id !== null ?
								<Button variant="primary" onClick={() => buy(productId)}>Buy</Button>
								:
								<Link className="btn btn-danger" to="/login">Log In</Link>
							}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}