import {useEffect,useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AddAddProduct(){

	const [show, setShow] = useState(false)
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);


	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isActiveBtn, setIsActiveBtn] = useState(false);

	function addNewProduct(e){
		 // e.preventDefault();

		fetch('http://localhost:4000/products/addProduct',{
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
		 				name: name,
		 				description: description,
		 				price: price
			 	})
		 	})	
		 	.then(res => res.json())
		 	.then(data => {
		 		console.log(data)
		 		if(data){
		 			Swal.fire({
		 				title: 'New Product Added Successfully!',
		 				icon: 'success',
		 				text: 'Add more products'
		 		});
		 		} else {
			 			Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
						text: 'Please try again later'
					});
		 		}
		 		
		 	})
		 	setName('');
			setDescription('');
			setPrice('');
		}
		
	

	useEffect(()=> {
		if(name !== '' && description !== '' && price!== '' && price !==0){
			setIsActiveBtn(true);
		} else {
			setIsActiveBtn(false);
		}
	},[name, description, price]);

	return(
		<>
		<Button variant="primary" onClick={handleShow}>Add New Product</Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Add New Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={e => addNewProduct(e)}>

            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter Product Here"
                value={name}
                autoFocus
                onChange={e => setName(e.target.value)}
              />
            </Form.Group>

            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Product Description</Form.Label>
              <Form.Control 
              as="textarea" 
              rows={3} 
              placeholder="Product Description Here"
              value={description}
              onChange={e => setDescription(e.target.value)}
              />
            </Form.Group>


            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Price"
                value={price}
                onChange={e => setPrice(e.target.value)}
                autoFocus
              />
            </Form.Group>


          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" type="submit" id="submitBtn" onClick={(e) => addNewProduct(e)}>
            Save New Product
          </Button>
        </Modal.Footer>
      </Modal>
      </>
	)




}