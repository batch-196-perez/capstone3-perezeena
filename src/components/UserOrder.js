// import  UserContext from '../UserContext';
// import {useState, useEffect,useContext} from 'react';
// import UserOrderCard from './UserOrder';
// import {Table} from 'react-bootstrap';

// export default function UserOrder(){


// 	const {user} = useContext(UserContext);
// 	const [orders, setOrders] = useState('')

// 	useEffect(() => {
// 		fetch('http://localhost:4000/users/getAlslOrder',{
// 			headers: {
// 				Authorization : `Bearer ${localStorage.getItem('token')}`
// 			}
// 		})
// 		.then(res => res.json())
// 		.then((data)=> {
// 			// console.log(data)
// 			setOrders(data.map(order => {
// 				return(
// 					<UserOrderCard key={order._id} orderProp={order}/>
// 				)
// 			}))
// 		})
// 	},[])

// 	return(
// 		// <h1>hello</h1>
// 		<>
// 		<Table striped bordered hover size="sm">
// 			<thead>
// 				<tr>
// 					<th>Order Id</th>
// 					<th>Product Id</th>
// 					<th>Qty</th>
// 				</tr>
// 			</thead>
// 			<tbody>
// 				{orders}
// 			</tbody>
// 		</Table>
// 		</>
// 	)
// }

import { useEffect, useState, useContext } from "react";
import { Button, Table } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import UserOrderCard from '../components/UserOrderCard';
import UserContext from '../UserContext';

export default function UserOrder() {
    const {user, setUser} = useContext(UserContext);
    const redirect = useNavigate();
    const [orders, setOrders] = useState([]); 
    
    useEffect(() => {
        fetch("http://localhost:4000/users/getAllOrder", {
             headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then((data) => {
            // console.log(data);

            setOrders(data.map(order => {
                return (
                    <UserOrderCard key={order._id} orderProp={order} />
                )
            }))
        })
    }, []);

    return (
        // (user.isAdmin) ?
        <>
            <Table bordered className="mt-5">
                <thead>
                    <tr>
                        <th>Customer Name</th>
                        <th>Product Id</th>
                        <th>Total Amount</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                    { orders }
                </tbody>
            </Table>
            <Button className="mt-5" variant="outline-dark">
                BACK TO DASHBOARD
            </Button> 
        </>
        // :
        // redirect('/home')
    );
}
