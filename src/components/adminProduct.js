import {useState, useEffect,useContext} from 'react';
import {Container, Col, Row, Table, Button} from 'react-bootstrap';
import AdminProductCard from '../components/AdminProductCard';
import AdminAddProduct from '../components/AdminAddProduct';
import UserContext from '../UserContext';

export default function AdminProductView(props){

	const {user} = useContext(UserContext);

	// const {breakpoint, productProp} = props
	// const {name, description, price, _id} = productProp;

		const [products, setProducts] = useState([]);
		const [show, setShow] = useState(false)
		const handleClose = () => setShow(false);
		const handleShow = () => setShow(true);
		

	useEffect(()=> {
		fetch('http://localhost:4000/products/getAllProducts',{
			method: 'GET',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			const productsArr = (data.map(product => {
				return(
				
					<AdminProductCard key={product._id} productProp = {product}  />
					
				)
			}))
			setProducts(productsArr)
		});
	},[]);

	return (
		<>
		
		<h1>Admin</h1>
		<Table striped hover>
      		<thead>
      		<tr>
      		<th colSpan={6} className="text-center mb-0"><h3 className="mb-0">PRODUCT TABLE</h3></th>
      		<th colSpan={2}  className="text-center"><AdminAddProduct/></th>
      		</tr>
      		<tr>
			       <th >NAME</th>
			       <th>DESCRIPTION</th>
			       <th>PRICE</th>
			       <th>STATUS</th>
			       <th>DELETE</th>
			       <th>UPDATE</th>
			       <th>DISABLE</th>
		        </tr>
      		</thead>
      		<tbody>	
		  	{products}
		  	</tbody>
    	</Table>
		
		</>
	)
}

