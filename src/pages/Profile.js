import {Container, Row, Col, Card, Table} from 'react-bootstrap';
import {useState, useEffect, useContext, Div} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import  UserContext from '../UserContext';
import {Link} from 'react-router-dom';
import UserOrder from '../components/UserOrder';
import '../App.css';

export default function Profile(){

	const {user} = useContext(UserContext);
	// console.log(user);

	const [profile, setProfile] = useState([]);

	const[firstName, setFirstName] = useState('');
	const[lastName, setLastName] = useState('');
	const[mobileNo, setMobileNo] = useState('');
	const[email, setEmail] = useState('');
	//const[products, setProduct] = useState('');
	const[orders, setOrders] = useState('');

	useEffect(() => {
		fetch('http://localhost:4000/users/userDetails',{
			headers: {
				Authorization : `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setFirstName(data.firstName);
			setLastName(data.lastName);
			setMobileNo(data.mobileNo);
			setEmail(data.email);
			// setOrders(data.orders);
			//setProduct(data.orders);
			
		});
	})


	return (

		
			(user.id !== null) ?
			<>
					<div className="profile mt-5">
						<div className="pro">
						<Row>
							<Col lg={3} className="profile-section">
								<h5>User's Profile</h5>
								<p className=""><b> {firstName}  {lastName}</b></p>
								<p className="text-center"><b>Details</b></p>
								<p className="">Mobile No: {mobileNo}</p>
								<p className="">Email: {email}</p>
							</Col>
							<Col lg={9}>
								<UserOrder/>
							</Col>
							</Row>		
						</div>

					</div>
			</>
			:
			<Navigate to="/products"/>
			
		)



}